import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class CGAiperi {
    WebDriver driver = SetUP.getWebDriver();

    @BeforeMethod
    public void setUp() {
        SetUP.getCarGurusURL(driver);

    }


    //"Select Model drop down button" modification!!!
    // Expected result : modified

    // Example: entered parameters
    //All Makers: "Porsche"
    //All Models: "Cayenne"
    //NEAR: "60618"
    //Modified car type: "BMW"

    //Test Steps
    //1. Click "Certified Pre-Owned" option
    //2. Fill up the scroll down  window
    //3. Press enter
    //4. If captcha appears in new window, should ignore it
    //5. Verify if selected car type in left side bar is same as initial selected
    //6. Verify if selected car model in left side bar is same as initial selected
    //7. Modify : select new car type
    //8. Verify if new selected car type is updated on car type bar
    //9. Verify if model drop down button --> Modified

    @Test(priority = 2)
    public void  modelDropDownModification() {
        driver.findElement(By.xpath("//label[@for='heroSearch-tab-2']")).click();

        WebElement car = driver.findElement(By.id("carPickerCpo_makerSelect"));
        car.click();
        Select cars = new Select(car);

        cars.selectByVisibleText("Porsche");

        WebElement models = driver.findElement(By.id("carPickerCpo_modelSelect"));
        models.click();

        Select model = new Select(models);
        model.selectByVisibleText("Cayenne");
        driver.findElement(By.id("dealFinderZipCPOId")).clear();
        driver.findElement(By.id("dealFinderZipCPOId")).sendKeys("60618" + Keys.ENTER);

        WebElement button = driver.findElement(By.xpath("//button[@type='button']"));

        if (button.isDisplayed()) {
            button.click();

            WebElement carType = driver.findElement(By.xpath("//select[@class='select maker-select-dropdown btn-small form-control ft-make-selector']"));

            Select carTypeButton = new Select(carType);


            Assert.assertTrue(carTypeButton.getFirstSelectedOption().getText().equals("Porsche"), "Selected car type Is FAILED");


            WebElement modelType = driver.findElement(By.xpath("//select[@class='select model-select-dropdown btn-small form-control ft-model-selector']"));

            Select modelTypeButton = new Select(modelType);


            Assert.assertTrue(modelTypeButton.getFirstSelectedOption().getText().equals("Cayenne"), "Selected car module Is FAILED");


            carType.click();
            carTypeButton.selectByVisibleText("BMW");


            Assert.assertTrue(carTypeButton.getFirstSelectedOption().getText().equals("BMW"), "Modified car type Is FAILED");


            Assert.assertTrue(modelTypeButton.getFirstSelectedOption().getText().equals("Select Model"), "Modified car model Is FAILED");

        }
    }


    //Test Steps
    //1. Click "Certified Pre-Owned" option
    //2. Fill up the scroll down  window
    //3. Press enter
    //4. If captcha appears in new window, should ignore it
    //5. New page should scroll down
    //6. On the left side bar click the "Automatic" button
    //7. Page should scroll up
    //8. Verify if left side from "Save Search" button appear new filter box "Automatic"
    //9. Click "ex" side of "Automatic" button
    //10. Verify if "Automatic" button is not on filter section more


    @Test(priority = 2)
    public void filterBox(){

        driver.findElement(By.xpath("//label[@for='heroSearch-tab-2']")).click();

        WebElement car =  driver.findElement(By.id("carPickerCpo_makerSelect"));
        car.click();
        Select cars = new Select(car);

        cars.selectByVisibleText("Porsche");

        WebElement models = driver.findElement(By.id("carPickerCpo_modelSelect"));
        models.click();

        Select model = new Select(models);
        model.selectByVisibleText("Cayenne");
        driver.findElement(By.id("dealFinderZipCPOId")).clear();
        driver.findElement(By.id("dealFinderZipCPOId")).sendKeys("60618"+ Keys.ENTER);

        WebElement button = driver.findElement(By.xpath("//button[@type='button']"));

        if(button.isDisplayed()) {
            button.click();


            ((JavascriptExecutor) driver).executeScript("scroll(0,800)");

            WebElement automaticButton = driver.findElement(By.xpath("//label[@id='option_auto']"));
            automaticButton.click();

            WebElement check = driver.findElement(By.xpath("//a//span[@class='caption']"));

            ((JavascriptExecutor) driver).executeScript("scroll(0,-1000)");


            Assert.assertTrue(check.getText().equals("Automatic"), "Selected button is FAILED");


            WebElement exButton = driver.findElement(By.xpath("//span[@style='padding-left:4px; opacity:.5; font-size:20px; line-height: 0.2; position:relative; top:1px;']"));
            exButton.click();

            WebElement anyButton = driver.findElement(By.xpath("//label[@class='btn btn-sm btn-default active']"));
            String attributeValue = anyButton.getAttribute("data-cg-value");


            Assert.assertTrue(attributeValue.equals("ANY"), "Attribute value button is FAILED");
        }
    }


    //Test Steps
    //1. Click "Certified Pre-Owned" option
    //2. Fill up the scroll down  window
    //3. Press enter
    //4. If captcha appears in new window, should ignore it

    //5. Scroll down
    //6. On the left side corner bar click the "English" button
    //8. From scrolled up option choose the "Espanol" button
    //10. Verify if page language changed to "Spanish"


    @Test(priority = 2)
    public void languageModification(){
        driver.findElement(By.xpath("//label[@for='heroSearch-tab-2']")).click();

        WebElement car =  driver.findElement(By.id("carPickerCpo_makerSelect"));
        car.click();
        Select cars = new Select(car);

        cars.selectByVisibleText("Porsche");

        WebElement models = driver.findElement(By.id("carPickerCpo_modelSelect"));
        models.click();

        Select model = new Select(models);
        model.selectByVisibleText("Cayenne");
        driver.findElement(By.id("dealFinderZipCPOId")).clear();
        driver.findElement(By.id("dealFinderZipCPOId")).sendKeys("60618"+ Keys.ENTER);

        WebElement button = driver.findElement(By.xpath("//button[@type='button']"));

        if(button.isDisplayed()) {
            button.click();

            ((JavascriptExecutor) driver).executeScript("scroll(0,5400)");

            WebElement englishButton = driver.findElement(By.xpath("//button[@class=\"btn btn-default dropdown-toggle\"]"));
            englishButton.click();

            WebElement spanishButton = driver.findElement(By.xpath("//p[@class='dropdown-subheader']/../ul/li/a"));
            spanishButton.click();

            WebElement spanishButton2 = driver.findElement(By.xpath("//button[@class=\"btn btn-default dropdown-toggle\"]"));

            Assert.assertTrue(spanishButton2.getAttribute("title").equals("Español"), "Button Language Case FAILED");
        }
    }



}




