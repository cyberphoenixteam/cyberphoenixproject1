


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;


import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class CGAltynai {

        WebDriver driver = SetUP.getWebDriver();

    @BeforeClass
    public void setUp() {
        SetUP.getCarGurusURL(driver);

    }

        @Test()

        public void Altynai() throws InterruptedException {

            WebElement questions = driver.findElement(By.linkText("Questions"));

            String  actualLinkText = questions.getText();
            String expectedLinkText = "Questions";

            Assert.assertTrue(actualLinkText.equals(expectedLinkText));

            questions.click();
        }
        @Test()
        public void Altynai2() throws InterruptedException {

            WebElement questions = driver.findElement(By.linkText("Questions"));

            String  actualLinkText = questions.getText();
            String expectedLinkText = "Questions";

            Assert.assertTrue(actualLinkText.equals(expectedLinkText));

            questions.click();

            WebElement questionsByType = driver.findElement(By.xpath("//*[@id='contentBody']/div/aside/div[3]/div/h2"));

            String actualHeading = questionsByType.getText();


            String expectedHeading = "Questions by Type";
            Assert.assertTrue(actualHeading.equals(actualHeading));

        }
        @Test
        public void Altynai3() throws InterruptedException {

            WebElement questions = driver.findElement(By.linkText("Questions"));

            String actualLinkText = questions.getText();
            String expectedLinkText = "Questions";
            
            Assert.assertTrue(actualLinkText.equals(expectedLinkText));

            questions.click();

            WebElement questionsByType = driver.findElement(By.xpath("//*[@id='contentBody']/div/aside/div[3]/div/h2"));

            String actualHeading = questionsByType.getText();


            String expectedHeading = "Questions by Type";
            List<WebElement> linksBellowQuestionsByType = driver.findElements(By.xpath("//*[@id=\"contentBody\"]/div/aside/div[3]//a"));
            int actualNumberOfLinks = linksBellowQuestionsByType.size();

            int expectedNumberOfLinks = 5;
            Assert.assertTrue(actualNumberOfLinks == expectedNumberOfLinks);


        }


    }

