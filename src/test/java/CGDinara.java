
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class CGDinara {

    WebDriver driver = SetUP.getWebDriver();
    /*

    TC by Dinara Aidarova
    TC1: Validation of the heading 'Popular Makers'
    TC2: Validating the number of links under 'Popular Makers's'
    TC3: Validating the links' texts
            */

    @BeforeClass
    public void setURL(){
        SetUP.getCarGurusURL(driver);
    }

    @Test(priority = 1)
    public void validationOfTheHeadingPopularMakers() {

        /*
        TC1:  Validation of the heading 'Popular Makers'
        1. Navigate to heading "Popular Makers"
        2. Check if actual heading matches to expected heading value
         */



        WebElement popularMakersHeading = driver.findElement(By.xpath("//section[@class='collapsible static popular-maker']//h4"));

        String actualResultOfHeading = popularMakersHeading.getAttribute("data-title");
        String expectedResultOfHeading = "Popular Makers";

        Assert.assertEquals(actualResultOfHeading,expectedResultOfHeading, "Validation of 'Popular Makers' heading FAILED!");

    }

    /*
    TC2: Validating the number of links under 'Popular Makers's'
    1. Navigate to the links below "Popular Makers"
    2. Compare the actual number of links to the expected number of links
     */
    @Test(priority = 1)
    public void validatingNumberOfLinksUnderPopularMakers() {
        List<WebElement> listOfCarsInPopularMakers = driver.findElements(By.xpath("//div[@id='popularMakers']//ul//a"));
        int actualNumberOfLinksOfCars = listOfCarsInPopularMakers.size();
        int expectedNumberOfLinksOfCars = 39;

        Assert.assertEquals(actualNumberOfLinksOfCars, expectedNumberOfLinksOfCars, "Validation of number of links under 'Popular Makers' FAILED!");
        {

        }

    }
    /*
TC3: Validating the links' texts
1. Create a list of expected texts of links
2. Store all the actual links to the another list
3. Compare if these two lists are equal
 */
    @Test(priority = 1)
    public void validatingTheLinkNames() {
        List<WebElement> listOfCarsInPopularMakers = driver.findElements(By.xpath("//div[@id='popularMakers']//ul//a"));

        List<String> expectedLinkTexts = (Arrays.asList("Acura", "Alfa Romeo", "Audi", "BMW", "Buick", "Cadillac", "Chevrolet",
                "Chrysler", "Dodge", "FIAT", "Ford", "Genesis", "GMC", "Honda", "Hummer", "Hyundai", "INFINITI", "Jaguar", "Jeep", "Kia",
                "Land Rover", "Lexus", "Lincoln", "Maserati", "Mazda", "Mercedes-Benz", "Mercury", "MINI", "Mitsubishi", "Nissan", "Pontiac", "Porsche",
                "Ram", "Saturn", "Scion", "Subaru", "Toyota", "Volkswagen", "Volvo"));

        List<String> actualLinkText = new ArrayList<String>();

        for (int i = 0; i < listOfCarsInPopularMakers.size(); i++) {

            actualLinkText.add(listOfCarsInPopularMakers.get(i).getText());

        }

        Assert.assertEquals(expectedLinkTexts,actualLinkText,"Validation of list of links' texts FAILED!");


    }

}
