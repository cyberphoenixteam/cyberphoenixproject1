import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class CGEldiar {

    WebDriver driver = SetUP.getWebDriver();

    @BeforeClass
    public void setURL(){
        SetUP.getCarGurusURL(driver);
    }

    @Test(priority = 5)
    public void A() throws InterruptedException{


//    driver.manage().window().fullscreen();
//
//    driver.manage().timeouts().implicitlyWait(12, TimeUnit.SECONDS);
//
//    driver.get("https://www.cargurus.com/");

//        Thread.sleep(2000);

        Actions myAction = new Actions(driver);

        driver.findElement(By.xpath("//a[@href=\"/Cars/new/\"]")).click();

//        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);

        WebElement element = driver.findElement(By.xpath("//select[@class='form-control cg-carPicker-makerSelect maker-select-dropdown ft-make-selector']"));

        Select select = new Select(element);

//        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);

        select.selectByVisibleText("McLaren");

        Assert.assertEquals(select.getFirstSelectedOption().getText(),"McLaren", "Mc Laren check");

//        Assert.assertTrue(select.getFirstSelectedOption().getText().equals("McLaren"));

//    if (select.getFirstSelectedOption().getText().equals("McLaren")) {
//
//        System.out.println("Make: McLaren is verfified");
//    } else {
//        System.out.println("Make: McLaren is not verified");
//    }
        WebElement element2 = driver.findElement(By.xpath("//select[@id='submitTopicForm_entitySelectingHelper_selectedEntity_modelSelect']"));

//        Thread.sleep(2000);

        Select select2 = new Select(element2);

//        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);

        select2.selectByVisibleText("720S");



        Assert.assertEquals(select2.getFirstSelectedOption().getText(),"720S", "720S check");

//        Assert.assertTrue(select2.getFirstSelectedOption().getText().equals("720S"));

//    if (select2.getFirstSelectedOption().getText().equals("720S")) {
//        System.out.println("Model: 720S is verified");
//    } else {
//        System.out.println("Model: 720S is not verified");
//    }

        WebElement element3 = driver.findElement(By.xpath("//select[@id='submitTopicForm_entitySelectingHelper_selectedEntity_year1Select']"));

        Select select3 = new Select(element3);

        select3.selectByIndex(2);


        Assert.assertEquals(select3.getFirstSelectedOption().getText(),"2019", "2019 year check");

//        Assert.assertTrue(select3.getFirstSelectedOption().getText().equals("2019"));

//    if (select3.getFirstSelectedOption().getText().equals("2019")) {
////        System.out.println("Year: 2019 is verified");
////    } else {
////        System.out.println("Year: is not verified");
////    }

        WebElement element4 = driver.findElement(By.xpath("//select[@id='submitTopicForm_entitySelectingHelper_selectedEntity_trimSelect']"));

        Select select4 = new Select(element4);

        select4.selectByVisibleText("Coupe RWD");


        Assert.assertEquals(select4.getFirstSelectedOption().getText(),"Coupe RWD", "Coupe RWD check");


//        Assert.assertTrue(select4.getFirstSelectedOption().getText().equals("Coupe RWD"));


//    if (select4.getFirstSelectedOption().getText().equals("Coupe RWD")) {
//        System.out.println("Trim Coupe RWD: is verifed");
//    } else {
//        System.out.println("Trim Coupe RWD: not verifed");
//    }

        WebElement elementZip = driver.findElement(By.name("zip"));
        elementZip.clear();
        elementZip.sendKeys("90025");

        driver.findElement(By.xpath("//input[@id='submitTopicForm_0']")).click();

//        Thread.sleep(3000);

        WebElement zip1 = driver.findElement(By.xpath("//input[@id='newSearchHeaderForm_NewCar_zip']"));
        zip1.clear();
        zip1.sendKeys("90405" + Keys.ENTER);

//        Thread.sleep(3000);

        WebElement zip2 = driver.findElement(By.id("newSearchHeaderForm_NewCar_zip"));

//        Thread.sleep(3000);

        System.out.println();


        try {

            Assert.assertTrue(zip2.getAttribute("value").equals("90405"));
        }catch (Exception e){
            System.out.println("Zip code issue");
        }


//    if(zip2.getAttribute("value").equals("90405")){
////        System.out.println("Zip code was successfully changed");
////    }else{
////        System.out.println("Zip code was not changed");
////    }

//        Thread.sleep(5000);
//        driver.close();

    }


    @Test(priority = 5)
    public void B() throws InterruptedException{

//    driver.manage().window().fullscreen();
//
//    driver.manage().timeouts().implicitlyWait(12, TimeUnit.SECONDS);
//
//    driver.get("https://www.cargurus.com/");

//        Thread.sleep(2000);

        Actions myAction = new Actions(driver);

        driver.findElement(By.xpath("//a[@href=\"/Cars/new/\"]")).click();

//        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);

        WebElement element = driver.findElement(By.xpath("//select[@class='form-control cg-carPicker-makerSelect maker-select-dropdown ft-make-selector']"));

        Select select = new Select(element);

//        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);

        select.selectByVisibleText("Mercedes-Benz");


        Assert.assertEquals(select.getFirstSelectedOption().getText(),"Mercedes-Benz", "Mercedes-Benz check" );

//    Assert.assertTrue(select.getFirstSelectedOption().getText().equals("Mercedes-Benz"));


//    if (select.getFirstSelectedOption().getText().equals("Mercedes-Benz")) {
//
//        System.out.println("Make: Mercedes-Benz is verified");
//    } else {
//        System.out.println("Make: Mercedes-Benz is not verified");
//    }


        WebElement element2 = driver.findElement(By.xpath("//select[@id='submitTopicForm_entitySelectingHelper_selectedEntity_modelSelect']"));

//        Thread.sleep(2000);

        Select select2 = new Select(element2);

//        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);

        select2.selectByVisibleText("S-Class");


        Assert.assertEquals(select2.getFirstSelectedOption().getText(),"S-Class");

//    Assert.assertTrue(select2.getFirstSelectedOption().getText().equals("S-Class"));

//    if (select2.getFirstSelectedOption().getText().equals("S-Class")) {
//        System.out.println("Model: S-Class is verified");
//    } else {
//        System.out.println("Model: S-Class is not verified");
//    }



        WebElement element3 = driver.findElement(By.xpath("//select[@id='submitTopicForm_entitySelectingHelper_selectedEntity_year1Select']"));

        Select select3 = new Select(element3);

        select3.selectByIndex(1);


        Assert.assertEquals(select3.getFirstSelectedOption().getText(),"2019", "2019 year check");

//    Assert.assertTrue(select3.getFirstSelectedOption().getText().equals("2019"));


//
//    if (select3.getFirstSelectedOption().getText().equals("2019")) {
//        System.out.println("Year: 2019 is verified");
//    } else {
//        System.out.println("Year: is not verified");
//    }


        WebElement element4 = driver.findElement(By.xpath("//select[@id='submitTopicForm_entitySelectingHelper_selectedEntity_trimSelect']"));

        Select select4 = new Select(element4);

        select4.selectByIndex(1);


        Assert.assertEquals(select4.getFirstSelectedOption().getText(),"Maybach S 560 4MATIC AWD", "Maybach S 560 4MATIC AWD check" );

//    Assert.assertTrue(select4.getFirstSelectedOption().getText().equals("Maybach S 560 4MATIC AWD"));

//
//    if (select4.getFirstSelectedOption().getText().equals("Maybach S 560 4MATIC AWD")) {
//        System.out.println("Trim Maybach S 560 4MATIC AWD: is verified");
//    } else {
//        System.out.println("Trim Maybach S 560 4MATIC AWD: not verified");
//    }

        driver.findElement(By.name("zip")).clear();
        try {
            driver.findElement(By.name("zip")).sendKeys("90025");

        }catch (Exception b){
            System.out.println("Zip code issue2");
        }
        driver.findElement(By.xpath("//input[@id='submitTopicForm_0']")).click();



//        Thread.sleep(3000);

        System.out.println();

        driver.findElement(By.name("zip")).clear();


        driver.findElement(By.xpath("//a[@onclick='trimFilter.clear();return false;']")).click();


//        Thread.sleep(2000);





        WebElement elementMaybach2 = driver.findElement(By.xpath("//input[@value='d82,Maybach S 560 4MATIC AWD']"));
        elementMaybach2.click();

        Assert.assertTrue(elementMaybach2.getAttribute("value").contains("Maybach S 560 4MATIC AWD"),"Maybach S 560 4MATIC AWD check");
//    if(elementMaybach2.getAttribute("value").contains("Maybach S 560 4MATIC AWD")){
//        System.out.println("Maybach S 560 4MATIC AWD Succesfully selected");
//    }else{
//        System.out.println("Maybach S 560 4MATIC AWD not selected");
//    }

//        Thread.sleep(1000);



        WebElement elementMaybach650 = driver.findElement(By.xpath("//input[@value='d82,Maybach S 650 RWD']"));


        elementMaybach650.click();

//        Thread.sleep(1000);

        Assert.assertTrue(elementMaybach650.getAttribute("value").contains("Maybach S 650 RWD"), "Maybach S 650 RWD check");

//    if(elementMaybach650.getAttribute("value").contains("Maybach S 650 RWD")){
//        System.out.println("Maybach S 650 RWD Succesfully selected");
//    }else{
//        System.out.println("Maybach S 650 RWD not selected");
//    }


//        Thread.sleep(1000);



        WebElement elementMaybach3 = driver.findElement(By.xpath("//input[@value='d82,S 450 RWD']"));
        elementMaybach3.click();


        Assert.assertTrue(elementMaybach3.getAttribute("value").contains("S 450 RWD"), "S 450 RWD check");

//    if(elementMaybach3.getAttribute("value").contains("S 450 RWD")){
//        System.out.println("S 450 RWD Successfully selected");
//    }else{
//        System.out.println("S 450 RWD not selected");
//    }

//
//        Thread.sleep(5000);
//        driver.close();
    }

    @Test(priority = 5)
    public void C() throws InterruptedException{
        Actions myAction = new Actions(driver);
        try{
            driver.findElement(By.xpath("//a[@href=\"/Cars/new/\"]")).click();
        }catch (Exception e){
            System.out.println("Link issue: First Method");
        }
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        WebElement element = driver.findElement(By.xpath("//select[@class='form-control cg-carPicker-makerSelect maker-select-dropdown ft-make-selector']"));
        Select select = new Select(element);
//      driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        select.selectByVisibleText("Lamborghini");
        Assert.assertEquals(select.getFirstSelectedOption().getText(),"Lamborghini","Lamborghini check"  );
//        if(select.getFirstSelectedOption().getText().equals("Lamborghini")){
//            System.out.println("Make: Lamborghini is verified");
//        }else{
//            System.out.println("Make is not selected");
//        }
        WebElement element2 = driver.findElement(By.xpath("//select[@id='submitTopicForm_entitySelectingHelper_selectedEntity_modelSelect']"));
//        Thread.sleep(2000);
        Select select2 = new Select(element2);
//        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
//        System.out.println(select2.getFirstSelectedOption().getText());
        select2.selectByIndex(1);
        Assert.assertEquals(select2.getFirstSelectedOption().getText(),"Aventador");
//        Assert.assertTrue(select2.getFirstSelectedOption().getText().equals("Aventador"));
//        if(select2.getFirstSelectedOption().getText().equals("Aventador")){
//            System.out.println("Model: Aventador is verified");
//        }else{
//            System.out.println("Model is not selected");
//        }
        WebElement element3 = driver.findElement(By.xpath("//select[@id='submitTopicForm_entitySelectingHelper_selectedEntity_year1Select']"));
        Select select3 = new Select(element3);
        select3.selectByIndex(1);
//        Thread.sleep(1000);
        Assert.assertEquals(select3.getFirstSelectedOption().getText(),"2019");
//        Assert.assertTrue(select3.getFirstSelectedOption().getText().equals("2019"));
//        if(select3.getFirstSelectedOption().getText().equals("2019")){
//            System.out.println("Year: 2019 is verified");
//        }else{
//            System.out.println("Year is not verified");
//        }
        WebElement element4 = driver.findElement(By.id("submitTopicForm_entitySelectingHelper_selectedEntity_trimSelect"));
        Select select4 = new Select(element4);
        select4.selectByIndex(1);
        driver.findElement(By.name("zip")).clear();
        driver.findElement(By.name("zip")).sendKeys("90025");
        driver.findElement(By.xpath("//input[@id='submitTopicForm_0']")).click();
//        Thread.sleep(3000);
        System.out.println();
        WebElement element1 = driver.findElement(By.xpath("//h4[@class='cg-dealFinder-result-model']"));
        element1.click();
//        Thread.sleep(2000);
//   123     WebElement elementX = driver.findElement(By.xpath("//p[@class='cg-listing-infoOverlay label-visited']"));
//        driver.navigate().back();
//        Thread.sleep(2000);
        driver.findElement(By.xpath("//a[@class='mb-scrollButtons mb-right']")).click();
//        driver.findElement(By.xpath("//span[@class='la la-arrow-left']")).click();
//        Thread.sleep(2000);
        driver.findElement(By.xpath("//a[@class='mb-scrollButtons mb-right']")).click();
//        Thread.sleep(2000);
        driver.findElement(By.xpath("//a[@class='mb-scrollButtons mb-right']")).click();
//        Thread.sleep(2000);
        driver.findElement(By.xpath("//a[@class='mb-scrollButtons mb-right']")).click();
//        Thread.sleep(2000);
        driver.findElement(By.xpath("//a[@class='mb-scrollButtons mb-right']")).click();
//        Thread.sleep(2000);
        driver.findElement(By.xpath("//a[@class='mb-scrollButtons mb-right']")).click();
//        Thread.sleep(2000);
        driver.findElement(By.xpath("//a[@class='mb-scrollButtons mb-right']")).click();
//        Thread.sleep(2000);
        driver.findElement(By.xpath("//div[@style='background-image: url(https://static.cargurus.com/images/forsale/2019/06/18/11/39/2019_lamborghini_aventador-pic-5284887306116666483-152x114.jpeg);background-position:50% 50%;background-size:cover;']")).click();
        Thread.sleep(2000);
        WebElement displayed =  driver.findElement(By.xpath("//div[@class='cg-listingDetail-mainPictureWrap cg-listingDetail-mainPictureWrapActive']"));
        displayed.click();
        Assert.assertTrue(displayed.isDisplayed());
//        if(displayed.isDisplayed()){
//            System.out.println("Image: displayed successfully");
//        }else{
//            System.out.println("Image: is not displayed");
//        }
//        Thread.sleep(5000);
//        driver.close();
    }
}