import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.List;

public class CGGabriel{
   WebDriver driver = SetUP.getWebDriver();
   SoftAssert softAssert = SetUP.getSoftAssert();

   @BeforeMethod
   public void setURL(){
      SetUP.getCarGurusURL(driver);
   }

   @Test(priority = 8)
   public void asUserIWantToSearchForUsedCarsByCar() {

      softAssert.assertTrue(Locators.goToUsedCars(driver).isDisplayed(),"USED CARS BUTTON NOT DISPLAYED");
      softAssert.assertTrue(Locators.goToUsedCars(driver).getText().equals("Used Cars"),"USED CARD BUTTON SYNTAX DOESN'T MATCH");
      Locators.goToUsedCars(driver).click();
      softAssert.assertTrue(driver.getTitle().contains("Used Cars"),"USED CARS TITLE MUST MATCH");

      //All Makes Box
      softAssert.assertTrue(Locators.goToAllMakes(driver).isEnabled(),"MAKES SELECT MENU SHOULD BE PRESENT");
      softAssert.assertTrue(Locators.getSelectFirstElement(Locators.goToAllMakes(driver)).getText().equals("All Makes"),"ALL MAKES MESSAGE SHOULD BE DISPLAYED");
      softAssert.assertTrue(Locators.getSelectOption(Locators.goToAllMakes(driver),"Ford"),"GIVEN VALUE AS FORD SHOULD BE SELECTED");
      softAssert.assertFalse(Locators.getSelectOption(Locators.goToAllMakes(driver),"Dacia"),"INVALID DATA ON MAKES SELECT");
      softAssert.assertEquals(143,Locators.getSelectDropDownSize(Locators.goToAllMakes(driver)),"INVALID NUMBER OF ELEMENTS INSIDE CAR MAKES SELECT");
      //All Model Box
      softAssert.assertTrue(Locators.goToAllModels(driver).isEnabled(),"MODEL SELECT MENU SHOULD BE PRESENT");
      softAssert.assertTrue(Locators.getSelectFirstElement(Locators.goToAllModels(driver)).getText().equals("Select Model"),"SELECT MENU SYNTAX SHOULD MATCH");
      softAssert.assertTrue(Locators.getSelectOption(Locators.goToAllModels(driver),"Explorer"),"GIVEN VALUE AS EXPLORER SHOULD BE SELECTED");
      softAssert.assertFalse(Locators.getSelectOption(Locators.goToAllModels(driver),"Camry"),"INVALID DATA ON MODEL SELECT");
      softAssert.assertEquals(107,Locators.getSelectDropDownSize(Locators.goToAllModels(driver)),"INVALID NUMBER OF ELEMENTS INSIDE MODEL SELECT");
      //First Year
      softAssert.assertTrue(Locators.goToFirstYearBox(driver).isEnabled(),"FIRST YEAR BUTTON SHOULD BE ENABLE");
      softAssert.assertTrue(Locators.getSelectFirstElement(Locators.goToFirstYearBox(driver)).getText().equals("All"),"FIRST YEAR BUTTON SHOULD HAVE ALL YEARS AS DISPLAY");
      softAssert.assertTrue(Locators.getSelectOption(Locators.goToFirstYearBox(driver),"2000"),"INVALID FIRST YEAR");
      softAssert.assertFalse(Locators.getSelectOption(Locators.goToFirstYearBox(driver),"1858"),"INVALID DATA ON YEAR SELECT");
      softAssert.assertEquals(31,Locators.getSelectDropDownSize(Locators.goToFirstYearBox(driver)),"INVALID NUMBER OF YEAR INSIDE SELECT");
      //Second Year
      softAssert.assertTrue(Locators.goToSecondYearBox(driver).isEnabled(),"SECOND YEAR BUTTON SHOULD BE ENABLE");
      softAssert.assertTrue(Locators.getSelectFirstElement(Locators.goToSecondYearBox(driver)).getText().equals("Similar"),"SECOND YEAR BUTTON SHOULD HAVE SIMILAR DISPLAY");
      softAssert.assertTrue(Locators.getSelectOption(Locators.goToSecondYearBox(driver),"2001"),"INVALID SECOND YEAR");
      softAssert.assertFalse(Locators.getSelectOption(Locators.goToSecondYearBox(driver),"1985"),"DATA ON YEAR SELECT IS TOO LOW");
      softAssert.assertEquals(31,Locators.getSelectDropDownSize(Locators.goToSecondYearBox(driver)),"INVALID NUMBER OF YEAR INSIDE SELECT");
      //Zip code
      softAssert.assertTrue(Locators.goToNewSearchZipCode(driver).isEnabled(),"ZIP CODE INPUT BOX SHOULD BE ENABLED");
      Locators.goToNewSearchZipCode(driver).clear();
      Locators.goToNewSearchZipCode(driver).sendKeys("55555");
      //Radius
      softAssert.assertTrue(Locators.goToNewSearchRadius(driver).isEnabled(),"SEARCH RADIUS BUTTON SHOULD BE ENABLE");
      softAssert.assertTrue(Locators.getSelectOption(Locators.goToNewSearchRadius(driver),"100 mi"),"INVALID SEARCH RADIUS");
      softAssert.assertFalse(Locators.getSelectOption(Locators.goToNewSearchRadius(driver),"1500"),"INVALID DATA OF SEARCH RADIUS");
      softAssert.assertEquals(9,Locators.getSelectDropDownSize(Locators.goToNewSearchRadius(driver)),"INVALID NUMBER OF ELEMENTS INSIDE SELECT");
      //Search complete
      Locators.goToSearchButton(driver).click();
      softAssert.assertTrue(Locators.saveSearch(driver).getText().equals("Save Search"));

      System.out.println("END OF FIRST TEST CASE: asUserIWantToSearchForUsedCarsByCar");
      softAssert.assertAll();

   }

   @Test(priority = 8)
   public void asUserIWantToSearchForUsedCarsByBodyStyle(){

      softAssert.assertTrue(driver.getCurrentUrl().equals("https://www.cargurus.com/"),"URL NOT MATCHING");
      softAssert.assertTrue(Locators.goToUsedCars(driver).isDisplayed(),"USED CARS BUTTON NOT DISPLAYED");
      softAssert.assertTrue(Locators.goToUsedCars(driver).getText().equals("Used Cars"),"USED CARD BUTTON SYNTAX DOESN'T MATCH");
      Locators.goToUsedCars(driver).click();
      softAssert.assertTrue(driver.getTitle().contains("Used Cars"),"USED CARS TITLE MUST MATCH");
      Locators.goToByBodyStyle(driver).click();
      //BODY Style
      softAssert.assertTrue(Locators.gotoByBody_BodyStyle(driver).isEnabled(),"BODY STYLE SELECT MENU SHOULD BE PRESENT");
      softAssert.assertTrue(Locators.getSelectFirstElement(Locators.gotoByBody_BodyStyle(driver)).getText().equals("Select Body Style"),"BODY STYLE DISPLAYED MESSAGE SHOULD BE DISPLAYED");
      softAssert.assertTrue(Locators.getSelectOption(Locators.gotoByBody_BodyStyle(driver),"Sedan"),"GIVEN VALUE AS SEDAN SHOULD BE SELECTED");
      softAssert.assertFalse(Locators.getSelectOption(Locators.gotoByBody_BodyStyle(driver),"Dacia"),"INVALID DATA ON BODY STYLE SELECT");
      softAssert.assertEquals(10,Locators.getSelectDropDownSize(Locators.gotoByBody_BodyStyle(driver)),"INVALID NUMBER OF ELEMENTS INSIDE BODY STYLE SELECT");
      //Zip Code
      softAssert.assertTrue(Locators.goToByBody_ZipCode(driver).isEnabled(),"ZIP CODE BOX SHOULD BE ENABLED");
      Locators.goToByBody_ZipCode(driver).clear();
      Locators.goToByBody_ZipCode(driver).sendKeys("55555");
      //Radius
      softAssert.assertTrue(Locators.goToByBody_Radius(driver).isEnabled(),"BY BODY RADIUS BUTTON SHOULD BE ENABLE");
      softAssert.assertTrue(Locators.getSelectOption(Locators.goToByBody_Radius(driver),"100 mi"),"INVALID SEARCH RADIUS");
      softAssert.assertFalse(Locators.getSelectOption(Locators.goToByBody_Radius(driver),"1500"),"INVALID DATA OF SEARCH RADIUS");
      softAssert.assertEquals(7,Locators.getSelectDropDownSize(Locators.goToByBody_Radius(driver)),"INVALID NUMBER OF ELEMENTS INSIDE SELECT RADIUS");
      //First Year
      softAssert.assertTrue(Locators.gotoByBody_FirstYear(driver).isEnabled(),"FIRST YEAR BUTTON SHOULD BE ENABLE");
      softAssert.assertTrue(Locators.getSelectFirstElement(Locators.gotoByBody_FirstYear(driver)).getText().equals("All"),"FIRST YEAR BUTTON SHOULD HAVE ALL AS DISPLAY");
      softAssert.assertTrue(Locators.getSelectOption(Locators.gotoByBody_FirstYear(driver),"2000"),"INVALID FIRST YEAR");
      softAssert.assertFalse(Locators.getSelectOption(Locators.gotoByBody_FirstYear(driver),"1858"),"INVALID DATA ON YEAR SELECT");
      softAssert.assertEquals(116,Locators.getSelectDropDownSize(Locators.gotoByBody_FirstYear(driver)),"INVALID NUMBER OF YEAR INSIDE SELECT");
      //Second Year
      softAssert.assertTrue(Locators.goToByBody_SecondYear(driver).isEnabled(),"SECOND YEAR BUTTON SHOULD BE ENABLE");
      softAssert.assertTrue(Locators.getSelectFirstElement(Locators.goToByBody_SecondYear(driver)).getText().equals("All"),"SECOND YEAR BUTTON SHOULD HAVE ALL DISPLAY");
      softAssert.assertTrue(Locators.getSelectOption(Locators.goToByBody_SecondYear(driver),"2001"),"INVALID SECOND YEAR");
      softAssert.assertFalse(Locators.getSelectOption(Locators.goToByBody_SecondYear(driver),"1885"),"DATA ON YEAR SELECT IS TOO LOW");
      softAssert.assertEquals(116,Locators.getSelectDropDownSize(Locators.goToByBody_SecondYear(driver)),"INVALID NUMBER OF YEAR INSIDE SELECT");
      //By First Price
      softAssert.assertTrue(Locators.goToByBody_FirstPrice(driver).isEnabled(),"FIRST PRICE BUTTON SHOULD BE ENABLE");
      softAssert.assertTrue(Locators.getSelectFirstElement(Locators.goToByBody_FirstPrice(driver)).getText().equals("---"),"FIRST PRICE BUTTON SHOULD HAVE --- DISPLAY");
      softAssert.assertTrue(Locators.getSelectOption(Locators.goToByBody_FirstPrice(driver),"$10,000"),"INVALID FIRST PRICE");
      softAssert.assertFalse(Locators.getSelectOption(Locators.goToByBody_FirstPrice(driver),"$300,000"),"DATA ON FIRST PRICE IS TOO HIGH");
      softAssert.assertEquals(45,Locators.getSelectDropDownSize(Locators.goToByBody_FirstPrice(driver)),"INVALID NUMBER PRICES INSIDE SELECT");
      //By Second Price
      softAssert.assertTrue(Locators.goToByBody_SecondPrice(driver).isEnabled(),"SECOND PRICE BUTTON SHOULD BE ENABLE");
      softAssert.assertTrue(Locators.getSelectFirstElement(Locators.goToByBody_SecondPrice(driver)).getText().equals("---"),"SECOND PRICE BUTTON SHOULD HAVE --- DISPLAY");
      softAssert.assertTrue(Locators.getSelectOption(Locators.goToByBody_SecondPrice(driver),"$10,000"),"INVALID SECOND PRICE");
      softAssert.assertFalse(Locators.getSelectOption(Locators.goToByBody_SecondPrice(driver),"$50"),"DATA ON SECOND PRICE IS TOO HIGH");
      softAssert.assertEquals(45,Locators.getSelectDropDownSize(Locators.goToByBody_SecondPrice(driver)),"INVALID NUMBER PRICES INSIDE SELECT");
      //By Max Mileage
      softAssert.assertTrue(Locators.goToByBody_MaxMileage(driver).isEnabled(),"MAX MILEAGE BUTTON SHOULD BE ENABLE");
      softAssert.assertTrue(Locators.getSelectFirstElement(Locators.goToByBody_MaxMileage(driver)).getText().equals("---"),"MAX MILEAGE BUTTON SHOULD HAVE --- DISPLAY");
      softAssert.assertTrue(Locators.getSelectOption(Locators.goToByBody_MaxMileage(driver),"100,000"),"INVALID MAX MILEAGE");
      softAssert.assertFalse(Locators.getSelectOption(Locators.goToByBody_MaxMileage(driver),"500"),"DATA ON MAX MILEAGE IS TOO LOW");
      softAssert.assertEquals(26,Locators.getSelectDropDownSize(Locators.goToByBody_MaxMileage(driver)),"INVALID NUMBER PRICES INSIDE SELECT");
      //Any
      softAssert.assertTrue(Locators.goToByBody_AnyRadioButton(driver).isEnabled(),"ANY BUTTON SHOULD BE ENABLE");
      Locators.goToByBody_AnyRadioButton(driver).click();
      softAssert.assertTrue(Locators.goToByBody_AnyRadioButton(driver).isSelected(),"ANY BUTTON SHOULD BE SELECTED");
      //Automatic
      softAssert.assertTrue(Locators.goToByBody_AutomaticRadioButton(driver).isEnabled(),"AUTOMATIC BUTTON SHOULD BE ENABLE");
      Locators.goToByBody_AutomaticRadioButton(driver).click();
      softAssert.assertTrue(Locators.goToByBody_AutomaticRadioButton(driver).isSelected(),"AUTOMATIC BUTTON SHOULD BE SELECTED");
      //Manual
      softAssert.assertTrue(Locators.goToByBody_ManualRadioButton(driver).isEnabled(),"MANUAL BUTTON SHOULD BE ENABLE");
      Locators.goToByBody_ManualRadioButton(driver).click();
      softAssert.assertTrue(Locators.goToByBody_ManualRadioButton(driver).isSelected(),"MANUAL BUTTON SHOULD BE SELECTED");
      //Search complete
      Locators.goToByBody_SearchButton(driver).click();
      System.out.println("END OF FIRST TEST CASE: asUserIWantToSearchForUsedCarsByBodyStyle");

      softAssert.assertAll();
   }
   @Test(priority = 8)
   public void asUserIWantToSearchForUsedCarsByPrice(){


      softAssert.assertTrue(driver.getCurrentUrl().equals("https://www.cargurus.com/"),"URL NOT MATCHING");
      softAssert.assertTrue(Locators.goToUsedCars(driver).isDisplayed(),"USED CARS BUTTON NOT DISPLAYED");
      softAssert.assertTrue(Locators.goToUsedCars(driver).getText().equals("Used Cars"),"USED CARD BUTTON SYNTAX DOESN'T MATCH");
      Locators.goToUsedCars(driver).click();
      softAssert.assertTrue(driver.getTitle().contains("Used Cars"),"USED CARS TITLE MUST MATCH");
      Locators.goToPrice(driver).click();
      //Zip code
      softAssert.assertTrue(Locators.goToPrice_ZipCode(driver).isEnabled(),"ZIP CODE INPUT BOX SHOULD BE ENABLED");
      Locators.goToPrice_ZipCode(driver).clear();
      Locators.goToPrice_ZipCode(driver).sendKeys("60601");
      //Radius
      softAssert.assertTrue(Locators.goToPrice_Radius(driver).isEnabled(),"BY PRICE RADIUS BUTTON SHOULD BE ENABLE");
      softAssert.assertTrue(Locators.getSelectOption(Locators.goToPrice_Radius(driver),"100 mi"),"INVALID SEARCH RADIUS");
      softAssert.assertFalse(Locators.getSelectOption(Locators.goToPrice_Radius(driver),"1500"),"INVALID DATA OF SEARCH RADIUS");
      softAssert.assertEquals(5,Locators.getSelectDropDownSize(Locators.goToPrice_Radius(driver)),"INVALID NUMBER OF ELEMENTS INSIDE SELECT RADIUS");
      //First Year
      softAssert.assertTrue(Locators.goToPrice_FirstYear(driver).isEnabled(),"FIRST YEAR BUTTON SHOULD BE ENABLE");
      softAssert.assertTrue(Locators.getSelectFirstElement(Locators.goToPrice_FirstYear(driver)).getText().equals("All"),"FIRST YEAR BUTTON SHOULD HAVE ALL AS DISPLAY");
      softAssert.assertTrue(Locators.getSelectOption(Locators.goToPrice_FirstYear(driver),"2000"),"INVALID FIRST YEAR");
      softAssert.assertFalse(Locators.getSelectOption(Locators.goToPrice_FirstYear(driver),"1858"),"INVALID DATA ON YEAR SELECT");
      softAssert.assertEquals(116,Locators.getSelectDropDownSize(Locators.goToPrice_FirstYear(driver)),"INVALID NUMBER OF YEAR INSIDE SELECT");
      //Second Year
      softAssert.assertTrue(Locators.goToPrice_SecondYear(driver).isEnabled(),"SECOND YEAR BUTTON SHOULD BE ENABLE");
      softAssert.assertTrue(Locators.getSelectFirstElement(Locators.goToPrice_SecondYear(driver)).getText().equals("All"),"SECOND YEAR BUTTON SHOULD HAVE ALL YEARS DISPLAY");
      softAssert.assertTrue(Locators.getSelectOption(Locators.goToPrice_SecondYear(driver),"2001"),"INVALID SECOND YEAR");
      softAssert.assertFalse(Locators.getSelectOption(Locators.goToPrice_SecondYear(driver),"1885"),"DATA ON YEAR SELECT IS TOO LOW");
      softAssert.assertEquals(116,Locators.getSelectDropDownSize(Locators.goToPrice_SecondYear(driver)),"INVALID NUMBER OF YEAR INSIDE SELECT");
      //By First Price
      softAssert.assertTrue(Locators.goToPrice_MinPrice(driver).isEnabled(),"FIRST PRICE BUTTON SHOULD BE ENABLE");
      softAssert.assertTrue(Locators.getSelectFirstElement(Locators.goToPrice_MinPrice(driver)).getText().equals("---"),"FIRST PRICE BUTTON SHOULD HAVE --- DISPLAY");
      softAssert.assertTrue(Locators.getSelectOption(Locators.goToPrice_MinPrice(driver),"$10,000"),"INVALID FIRST PRICE");
      softAssert.assertFalse(Locators.getSelectOption(Locators.goToPrice_MinPrice(driver),"$300,000"),"DATA ON FIRST PRICE IS TOO HIGH");
      softAssert.assertEquals(45,Locators.getSelectDropDownSize(Locators.goToPrice_MinPrice(driver)),"INVALID NUMBER PRICES INSIDE SELECT");
      //By Second Price
      softAssert.assertTrue(Locators.goToPrice_MaxPrice(driver).isEnabled(),"SECOND PRICE BUTTON SHOULD BE ENABLE");
      softAssert.assertTrue(Locators.getSelectFirstElement(Locators.goToPrice_MaxPrice(driver)).getText().equals("---"),"SECOND PRICE BUTTON SHOULD HAVE --- DISPLAY");
      softAssert.assertTrue(Locators.getSelectOption(Locators.goToPrice_MaxPrice(driver),"$10,000"),"INVALID SECOND PRICE");
      softAssert.assertFalse(Locators.getSelectOption(Locators.goToPrice_MaxPrice(driver),"$50"),"DATA ON SECOND PRICE IS TOO HIGH");
      softAssert.assertEquals(45,Locators.getSelectDropDownSize(Locators.goToPrice_MaxPrice(driver)),"INVALID NUMBER PRICES INSIDE SELECT");
      //By Max Mileage
      softAssert.assertTrue(Locators.goToPrice_MaxMileage(driver).isEnabled(),"MAX MILEAGE BUTTON SHOULD BE ENABLE");
      softAssert.assertTrue(Locators.getSelectFirstElement(Locators.goToPrice_MaxMileage(driver)).getText().equals("---"),"MAX MILEAGE BUTTON SHOULD HAVE --- DISPLAY");
      softAssert.assertTrue(Locators.getSelectOption(Locators.goToPrice_MaxMileage(driver),"100,000"),"INVALID MAX MILEAGE");
      softAssert.assertFalse(Locators.getSelectOption(Locators.goToPrice_MaxMileage(driver),"500"),"DATA ON MAX MILEAGE IS TOO LOW");
      softAssert.assertEquals(26,Locators.getSelectDropDownSize(Locators.goToPrice_MaxMileage(driver)),"INVALID NUMBER PRICES INSIDE SELECT");
      //Any
      softAssert.assertTrue(Locators.goToPrice_AnyRadioButton(driver).isEnabled(),"ANY BUTTON SHOULD BE ENABLE");
      Locators.goToPrice_AnyRadioButton(driver).click();
      softAssert.assertTrue(Locators.goToPrice_AnyRadioButton(driver).isSelected(),"ANY BUTTON SHOULD BE SELECTED");
      //Automatic
      softAssert.assertTrue(Locators.goToPrice_AutomaticRadioButton(driver).isEnabled(),"AUTOMATIC BUTTON SHOULD BE ENABLE");
      Locators.goToPrice_AutomaticRadioButton(driver).click();
      softAssert.assertTrue(Locators.goToPrice_AutomaticRadioButton(driver).isSelected(),"AUTOMATIC BUTTON SHOULD BE SELECTED");
      //Manual
      softAssert.assertTrue(Locators.goToPrice_ManualRadioButton(driver).isEnabled(),"MANUAL BUTTON SHOULD BE ENABLE");
      Locators.goToPrice_ManualRadioButton(driver).click();
      softAssert.assertTrue(Locators.goToPrice_ManualRadioButton(driver).isSelected(),"MANUAL BUTTON SHOULD BE SELECTED");
      //Search complete
      Locators.goToPrice_SearchButton(driver).click();
      System.out.println("END OF FIRST TEST CASE: asUserIWantToSearchForUsedCarsByPrice");
      softAssert.assertAll();

   }
   @AfterSuite
   public void close(){
      SetUP.closeWebDriver();
   }
}
class  Locators {

   public static WebElement element;


   public static WebElement goToUsedCars(WebDriver driver){
      element = driver.findElement(By.xpath("//a[@title='Used Cars']"));
      return element;
   }

   public static WebElement goToNewSearchBox(WebDriver driver){
      element = driver.findElement(By.xpath("//div[@class='panel-heading']"));
      return element;
   }
    /*
        BY CAR
     */

   public static WebElement goToAllMakes(WebDriver driver){
      element = driver.findElement(By.xpath("//select[@class='select maker-select-dropdown btn-small form-control ft-make-selector']"));
      return element;
   }

   public static WebElement goToAllModels(WebDriver driver){
      element = driver.findElement(By.xpath("//select[@class='select model-select-dropdown btn-small form-control ft-model-selector']"));
      return element;
   }

   public static WebElement goToFirstYearBox(WebDriver driver){
      element = driver.findElement(By.xpath("//select[@cg-name='ign-carId-entitySelectingHelper.selectedEntity']"));
      return element;
   }

   public static WebElement goToSecondYearBox(WebDriver driver){
      element = driver.findElement(By.xpath("//select[@cg-name='ign-car2Id-entitySelectingHelper.selectedEntity']"));
      return element;
   }

   public static WebElement goToNewSearchZipCode(WebDriver driver){
      element = driver.findElement(By.xpath("//td[@class='tdContent']//input[@name='zip']"));
      return element;
   }

   public static WebElement goToNewSearchRadius(WebDriver driver){
      element = driver.findElement(By.xpath("//select[@id='newSearchHeaderForm_UsedCar_distance']"));
      return element;
   }

   public static WebElement goToSearchButton(WebDriver driver){
      element = driver.findElement(By.xpath("//form[@id='newSearchHeaderForm_UsedCar']//input[@value='Search']"));
      return element;
   }

   public static WebElement saveSearch(WebDriver driver){
      element = driver.findElement(By.id("saveSearchButtonLabel"));
      return element;
   }
   /*
   By BODY
    */
   public static WebElement goToByBodyStyle(WebDriver driver){
      element = driver.findElement(By.xpath("//label[@data-value='bodyStyle']"));
      return element;
   }

   public static WebElement gotoByBody_BodyStyle(WebDriver driver){
      element = driver.findElement(By.id("bodyTypeGroup"));
      return element;
   }

   public static WebElement goToByBody_ZipCode(WebDriver driver){
      element = driver.findElement(By.xpath("//form[@id='newBodyStyleSearchHeaderForm']//input[@name='zip']"));
      return element;
   }

   public static WebElement goToByBody_Radius(WebDriver driver){
      element = driver.findElement(By.id("newBodyStyleSearchHeaderForm_distance"));
      return element;
   }

   public static WebElement gotoByBody_FirstYear(WebDriver driver){
      element = driver.findElement(By.xpath("//form[@id='newBodyStyleSearchHeaderForm']//select[@name='startYear']"));
      return element;
   }

   public static WebElement goToByBody_SecondYear(WebDriver driver){
      element = driver.findElement(By.xpath("//form[@id='newBodyStyleSearchHeaderForm']//select[@name='endYear']"));
      return element;
   }

   public static WebElement goToByBody_FirstPrice(WebDriver driver){
      element = driver.findElement(By.xpath("//form[@id='newBodyStyleSearchHeaderForm']//select[@name='minPrice']"));
      return element;
   }

   public static WebElement goToByBody_SecondPrice(WebDriver driver){
      element = driver.findElement(By.xpath("//form[@id='newBodyStyleSearchHeaderForm']//select[@name='maxPrice']"));
      return element;
   }

   public static WebElement goToByBody_MaxMileage(WebDriver driver){
      element = driver.findElement(By.xpath("//form[@id='newBodyStyleSearchHeaderForm']//select[@name='maxMileage']"));
      return element;
   }
   public static WebElement goToByBody_AnyRadioButton(WebDriver driver){
      element = driver.findElement(By.xpath("//form[@id='newBodyStyleSearchHeaderForm']//input[@value='ANY']"));
      return element;
   }

   public static WebElement goToByBody_AutomaticRadioButton(WebDriver driver){
      element = driver.findElement(By.xpath("//form[@id='newBodyStyleSearchHeaderForm']//input[@value='A']"));
      return element;
   }

   public static WebElement goToByBody_ManualRadioButton(WebDriver driver){
      element = driver.findElement(By.xpath("//form[@id='newBodyStyleSearchHeaderForm']//input[@value='M']"));
      return element;
   }
   public static WebElement goToByBody_SearchButton(WebDriver driver){
      element = driver.findElement(By.xpath("//form[@id='newBodyStyleSearchHeaderForm']//input[@value='Search']"));
      return element;
   }
   /*
   BY PRICE
    */
   public static WebElement goToPrice(WebDriver driver){
      element = driver.findElement(By.xpath("//label[@data-value='price']"));
      return element;
   }
   public static WebElement goToPrice_ZipCode(WebDriver driver){
      element = driver.findElement(By.xpath("//form[@id='newPriceSearchHeaderForm']//input[@name='zip']"));
      return element;
   }
   public static WebElement goToPrice_Radius(WebDriver driver){
      element = driver.findElement(By.xpath("//form[@id='newPriceSearchHeaderForm']//select[@name='distance']"));
      return element;
   }
   public static WebElement goToPrice_FirstYear(WebDriver driver){
      element = driver.findElement(By.xpath("//form[@id='newPriceSearchHeaderForm']//select[@name='startYear']"));
      return element;
   }
   public static WebElement goToPrice_SecondYear(WebDriver driver){
      element = driver.findElement(By.xpath("//form[@id='newPriceSearchHeaderForm']//select[@name='endYear']"));
      return element;
   }
   public static WebElement goToPrice_MinPrice(WebDriver driver){
      element = driver.findElement(By.xpath("//form[@id='newPriceSearchHeaderForm']//select[@name='minPrice']"));
      return element;
   }
   public static WebElement goToPrice_MaxPrice(WebDriver driver){
      element = driver.findElement(By.xpath("//form[@id='newPriceSearchHeaderForm']//select[@name='maxPrice']"));
      return element;
   }
   public static WebElement goToPrice_MaxMileage(WebDriver driver){
      element = driver.findElement(By.xpath("//form[@id='newPriceSearchHeaderForm']//select[@name='maxMileage']"));
      return element;
   }
   public static WebElement goToPrice_AnyRadioButton(WebDriver driver){
      element = driver.findElement(By.xpath("//form[@id='newPriceSearchHeaderForm']//input[@value='ANY']"));
      return element;
   }
   public static WebElement goToPrice_AutomaticRadioButton(WebDriver driver){
      element = driver.findElement(By.xpath("//form[@id='newPriceSearchHeaderForm']//input[@value='A']"));
      return element;
   }
   public static WebElement goToPrice_ManualRadioButton(WebDriver driver){
      element = driver.findElement(By.xpath("//form[@id='newPriceSearchHeaderForm']//input[@value='M']"));
      return element;
   }
   public static WebElement goToPrice_SearchButton(WebDriver driver){
      element = driver.findElement(By.xpath("//form[@id='newPriceSearchHeaderForm']//input[@value='Search']"));
      return element;
   }
   /*
   DROP DOWN
    */
   public static WebElement getSelectFirstElement(WebElement e){
      Select select = new Select(e);
      element = select.getFirstSelectedOption();
      return element;
   }

   public static int getSelectDropDownSize(WebElement element){
      Select select = new Select(element);
      List<WebElement> list = select.getOptions();
      return list.size();
   }

   public static boolean getSelectOption(WebElement element, String str){
      Select select = new Select(element);
      try{
         select.selectByVisibleText(str);
      }catch (Exception e){
         return false;
      }
      return true;
   }
}







