import com.github.javafaker.Faker;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import java.util.List;
import java.util.Locale;



public class CGMarianna { WebDriver driver = SetUP.getWebDriver();


        @BeforeClass
        public void setURL(){ SetUP.getCarGurusURL(driver); }

        @Test(priority = 6)
        public void ZipCode1() {

            WebElement sellMyCar = driver.findElement(By.xpath("//a[@title='Sell My Car']//span"));

            sellMyCar.click();

            WebElement sellYourCarButton =  driver.findElement(By.xpath("//button[@class = 'btn btn-primary btn-lg']"));

            sellYourCarButton.click();

            WebElement zipCodeField = driver.findElement(By.className("cg-sellACar-form__text-field-label"));

            String actualZIPText = zipCodeField.getText();
            String expectedZIPText = "ZIP";

            WebElement inputZipCode = driver.findElement(By.name("carDescription.postalCode"));

            Faker faker = new Faker(new Locale("en-US"));

            Assert.assertEquals(actualZIPText, expectedZIPText, "FAILED!");
            inputZipCode.sendKeys(faker.address().zipCodeByState("IL").substring(0,5));
        }
        @Test(priority = 6,dependsOnMethods = "ZipCode1")
        public  void checkDefaultValueInSelectMenu2 () {

            List<WebElement> defaultValueRightColumn = driver.findElements(By.xpath("//div[@class='cg-sellACar-form__carPicker']//div//div//select"));


            for (WebElement defaultValuesRight : defaultValueRightColumn) {
                Select rightColumnsText = new Select(defaultValuesRight);
                System.out.println(rightColumnsText.getFirstSelectedOption().getText());

                Assert.assertTrue(rightColumnsText.getFirstSelectedOption().getText().contains("Select Make")||rightColumnsText.getFirstSelectedOption().getText().contains("Select Model")
                        ||   rightColumnsText.getFirstSelectedOption().getText().contains("Select Year") || rightColumnsText.getFirstSelectedOption().getText().contains("Select Trim "), "Right column Default Text Verification FAILED");
            }

            List<WebElement> defaultValueLeftColumn = driver.findElements(By.xpath("//div[@class = 'col-xs-5']//div//div//select"));

            for (WebElement defaultValuesLeft : defaultValueLeftColumn) {

                Select leftColumnsText = new Select(defaultValuesLeft);

                System.out.println(leftColumnsText.getFirstSelectedOption().getText());

                Assert.assertTrue(leftColumnsText.getFirstSelectedOption().getText().contains("Select Transmission ") ||
                        leftColumnsText.getFirstSelectedOption().getText().contains("Select Engine (Optional)"),"Left column  Default Text Verification FAILED" );
            }

        }
        @Test(priority = 6,dependsOnMethods = "checkDefaultValueInSelectMenu2")
        public void SavingPostCarForSale3() throws InterruptedException{

            Faker faker = new Faker(new Locale("en-US"));

            List<WebElement> rightColumnOption = driver.findElements(By.xpath("//div[@class='cg-sellACar-form__carPicker']//div//div//select"));

            for (int i =0; i < rightColumnOption.size(); i++) {

                Select getLexus = new Select(rightColumnOption.get(0));
                getLexus.selectByVisibleText("Lexus");

                Select getModel = new Select(rightColumnOption.get(1));
                getModel.selectByVisibleText("IS 250");

                Select getYear = new Select(rightColumnOption.get(2));
                getYear.selectByVisibleText("2014");

                Select getTrim = new Select(rightColumnOption.get(3));
                getTrim.selectByValue("t82984");

            }

            Thread.sleep(2000);
            WebElement chooseMileage = driver.findElement(By.name("carDescription.mileage"));
//          Assert.assertTrue(chooseMileage.getText().contains("Mileage"), "No Default Value");
            chooseMileage.sendKeys("17000");


            WebElement payingOff = driver.findElement(By.xpath("//div[@class='ft-sellcar-lien-wrapper']/div/div/div[1]"));
            payingOff.click();

            WebElement exteriorColor = driver.findElement(By.name("exteriorColor"));
            exteriorColor.sendKeys("White");

            WebElement interiorColor = driver.findElement(By.name("interiorColor"));
            interiorColor.sendKeys("Black");

            WebElement Description= driver.findElement(By.xpath("//label[@class = 'cg-sellACar-form__text-field-input-wrapper']//textarea"));
            Description.sendKeys(faker.lorem().sentence(38));

            WebElement firstName = driver.findElement(By.name("firstName"));
            firstName.sendKeys(faker.name().firstName());
            WebElement lastName = driver.findElement(By.name("lastName"));
            lastName.sendKeys(faker.name().lastName());
            WebElement textMessage = driver.findElement(By.className("cg-sellACar-form__checkbox-check"));
            textMessage.click();
            WebElement phoneNumber = driver.findElement(By.name("sellerPhoneNumber"));
            phoneNumber.sendKeys(faker.phoneNumber().cellPhone());

            WebElement agreement =driver.findElement(By.xpath("//div[@class='ft-sellcar-disclaimer-wrapper']/div/div/div[1]"));
            agreement.click();



            JavascriptExecutor js = (JavascriptExecutor)driver;
            js.executeScript("document.getElementsByClassName(\"btn btn-sm btn-primary save\")[0].click()");
//      Assert.assertTrue(saveDescription.getText().contains("Saved!"), "Failed!");
        }

    }

