import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;
public class CGRaikhan {
    WebDriver driver = SetUP.getWebDriver();

    /*
 TC-1
  1. Click on "Research"
  2. USer should be able to see and open Research page
  3. User should be able to see "Research" text
  4. Click on "Home".User should be able to see and open Home page
  5. User should be able to see "Research New and Used Cars" text
  6. User should be able to see "Choose a car" text
  7.Click on Select Make. User should be able to click and see the options
  8.User should be able to select one of options
  */
    @BeforeClass
    public void setURL(){ SetUP.getCarGurusURL(driver); }

    @Test(priority = 4)
    public  void researchMethod1 () throws InterruptedException {

        driver.findElement(By.xpath("//a[@href='/Cars/autos/']/span[1]")).click();
        System.out.println("Research page displayed");
        System.out.println(driver.findElement(By.xpath("//li[@class='active']")).getText());


        Assert.assertTrue(driver.findElement(By.xpath("//a[@name='breadcrumb']")).isDisplayed()) ;
        driver.findElement(By.xpath("//a[@name='breadcrumb']")).click();
        System.out.println("Home Page clickable and displayed");

        driver.navigate().back();


        String s = driver.findElement(By.xpath("//h1[@class='cg-research-page-heading']")).getText();
        System.out.println(s);

        String s2 = driver.findElement(By.xpath("//h2[@class='cg-research-section-heading']")).getText();
        System.out.println(s2);

        WebElement selectMake = driver.findElement(By.xpath("//select[contains(@id,'researchTabCarSelector')]"));
        Select selectMakeDropdown = new Select(selectMake);
        System.out.println(selectMakeDropdown.getFirstSelectedOption().getText());
        selectMakeDropdown.selectByIndex(1);

    }
    @Test(priority = 4)
    public  void researchMethod2 ()throws InterruptedException{
             /*
        TC-3
        1.Click on Select Model
        2. User should be able to click and see the options
        3. User should be able to select the model
         */

        WebElement SelectModel = driver.findElement(By.xpath("//select[@id='researchTabCarSelector_entitySelectingHelper_selectedEntity_modelSelect']"));
        Select SelectModelDropdown = new Select(SelectModel);
        System.out.println(SelectModelDropdown.getFirstSelectedOption().getText());
        SelectModelDropdown.selectByIndex(2);
    }

    @Test(priority = 4)
    public  void researchMethod3 ()throws InterruptedException{
        /*
        TC-4
        1.Click on Select Year
        2. User should be able to click and see the options
        3. User should be able to select the year
        4. Click on Search
        5. User should be able to search
         */


        WebElement SelectYear = driver.findElement(By.xpath("//select[@id='researchTabCarSelector_entitySelectingHelper_selectedEntity_year1Select']"));
        Select SelectYearDropdown = new Select(SelectYear);
        System.out.println(SelectYearDropdown.getFirstSelectedOption().getText());
        SelectYearDropdown.selectByIndex(4);

        driver.findElement(By.xpath("//input[@type='submit']")).click();

    }
}
