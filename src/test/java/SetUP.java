import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.asserts.SoftAssert;

import java.util.concurrent.TimeUnit;

public class SetUP {

    private static SetUP setUp;

    private static WebDriver webDriver;

    private static SoftAssert softAssert;

    private SetUP(){

        WebDriverManager.chromedriver().setup();
        softAssert = new SoftAssert();
        webDriver = new ChromeDriver();

        webDriver.manage().window().fullscreen();
        webDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

    }


    public static SetUP setUpSingleton(){
        if(setUp == null){
            setUp = new SetUP();
        }
        return setUp;
    }
    public static WebDriver getWebDriver(){
        setUpSingleton();
        return webDriver;
    }

    public static SoftAssert getSoftAssert(){
        return softAssert;
    }
    public static void getCarGurusURL(WebDriver driver){
        webDriver.get("https://www.cargurus.com/");
    }

    public static void closeWebDriver(){
        if(webDriver != null){
            webDriver.close();
        }
    }

}